﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class kanistra : MonoBehaviour
{
    public GameObject kanistr;
    
    // Start is called before the first frame update
    void Start()
    {
        kanistr.SetActive(false);
        StartCoroutine(LoadingDisp(0.3f));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    IEnumerator LoadingDisp(float deltatime)
    {
        bool flag = true;
        while (true)
        {
            yield return new WaitForSeconds((float)Random.Range(0, 10)); //забирает управление на кол-во секунд (др. словами пауза)
            kanistr.SetActive(flag);
            flag = !flag;
        }
       // yield return null;
    }
}
